﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seek : SteeringBehaviour {

    public Vector3 target;
    public GameObject targetObj;


    private void OnDrawGizmos()
    {
        if(Application.isPlaying)
        {
            Debug.DrawLine(transform.position, target);
        }
    }

    // Use this for initialization
    void Start () {
        targetObj = GameObject.Find("SeekTarget");
	}
	
	// Update is called once per frame
	void Update () {
		if(targetObj != null)
        {
            target = targetObj.transform.position;
        }
	}

    public override Vector3 Calculate()
    {
        return boid.SeekForce(target);
    }
}
