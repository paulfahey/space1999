﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EagleSpawner : MonoBehaviour {

    public float gap = 20;
    public float followers = 2;
    public GameObject prefab;
    public GameObject spawnPos;
    float LeftSpawnGap;
    float RightSpawnGap;

    private void Awake()
    {
        StartCoroutine(SpawnEagles());
    }


    public IEnumerator SpawnEagles()
    {
        GameObject leader = (GameObject)Instantiate(prefab, spawnPos.transform.position, Quaternion.identity);
        leader.gameObject.name = "Leader";
        leader.AddComponent<Seek>();

        LeftSpawnGap += gap;
        RightSpawnGap += gap;

        for (int i = 0; i < followers; i++)
        {
            GameObject EagleClone = (GameObject)Instantiate(prefab, new Vector3(spawnPos.transform.position.x + LeftSpawnGap,
                spawnPos.transform.position.y, spawnPos.transform.position.z + LeftSpawnGap), Quaternion.identity);
            LeftSpawnGap += gap;
            EagleClone.AddComponent<OffsetPursuit>();
        }

        for (int j = 0; j < followers; j++)
        {
            GameObject EagleClone = (GameObject)Instantiate(prefab, new Vector3(spawnPos.transform.position.x - RightSpawnGap,
                spawnPos.transform.position.y, spawnPos.transform.position.z + RightSpawnGap), Quaternion.identity);
            RightSpawnGap += gap;
            EagleClone.AddComponent<OffsetPursuit>();
        }

        yield return new WaitForEndOfFrame();
    }
}
