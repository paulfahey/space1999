﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameObject target;
    private Vector3 targetPos;
    public float offset;

    void Start()
    {
        target = GameObject.Find("Leader");
    }

	// Update is called once per frame
	void Update () {
        Follow();
	}

    void Follow()
    {
        transform.LookAt(target.transform.position);
    }
}
