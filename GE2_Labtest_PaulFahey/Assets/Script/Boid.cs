﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boid : MonoBehaviour {

    public List<SteeringBehaviour> behaviours = new List<SteeringBehaviour>();

    public Vector3 force;
    public Vector3 acceleration;
    public Vector3 velocity;
    public int mass = 1;
    public float maxSpeed = 5f;

    // Use this for initialization
    void Start()
    {
        SteeringBehaviour[] behaviourArr = GetComponents<SteeringBehaviour>();

        foreach(SteeringBehaviour b in behaviourArr)
        {
            behaviours.Add(b);
        }
    }

    public Vector3 SeekForce(Vector3 target)
    {
        Vector3 desired = transform.position - target;
        desired.Normalize();
        desired *= maxSpeed;
        return desired - velocity;
    }

    public Vector3 ArriveForce(Vector3 target, float stoppingDistance, float deceleration)
    {
        Vector3 desired = transform.position - target;
        float distance = desired.magnitude;
        if (distance == 0)
        {
            return Vector3.zero;
        }
        float ramped = maxSpeed * (distance / stoppingDistance * deceleration);
        float clamped = Mathf.Min(ramped, maxSpeed);
        desired = clamped * (desired / distance);
        return desired - velocity;
    }

    Vector3 Calculate()
    {
        force = Vector3.zero;
        foreach(SteeringBehaviour b in behaviours)
        {
            if(b.isActiveAndEnabled)
            {
                force = b.Calculate() * b.weight;
            }
        }
        return force;
    }

    // Update is called once per frame
    void Update () {
        force = Calculate();
        Vector3 newAcceleration = force / mass;

        float smoothRate = Mathf.Clamp(9.0f * Time.deltaTime, .15f, .04f) / 2;
        acceleration = Vector3.Lerp(acceleration, newAcceleration, smoothRate);
        velocity += acceleration * Time.deltaTime;
        velocity = Vector3.ClampMagnitude(velocity, maxSpeed);

        transform.position -= velocity * Time.deltaTime;
	}
}
