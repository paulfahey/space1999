﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetPursuit : SteeringBehaviour {

    public Vector3 offset;
    public Boid leader;
    public Vector3 target;

    private void OnDrawGizmos()
    {
        if(Application.isPlaying && leader != null)
        {
            Debug.DrawLine(transform.position,leader.transform.position);
        }
    }

    // Use this for initialization
    void Start () {
        leader = GameObject.Find("Leader").GetComponent<Boid>();
        offset = transform.position - leader.transform.position;
        offset = Quaternion.Inverse(leader.transform.rotation) * offset;
	}
	
    public override Vector3 Calculate()
    {
        target = leader.transform.TransformPoint(offset);
        float distance = Vector3.Distance(target, transform.position);
        float time = distance / boid.maxSpeed;
        Vector3 targetPos = target + (-leader.velocity * time);
        return boid.ArriveForce(targetPos,0.1f,1f);
    }
}
